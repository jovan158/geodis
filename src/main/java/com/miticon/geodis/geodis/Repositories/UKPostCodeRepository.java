package com.miticon.geodis.geodis.Repositories;

import com.miticon.geodis.geodis.Entities.UKPostCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UKPostCodeRepository extends JpaRepository<UKPostCode, Long> {

    Optional<UKPostCode> findByPostcode(String postcode);
}
