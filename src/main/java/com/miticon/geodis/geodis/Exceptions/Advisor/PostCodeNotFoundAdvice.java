package com.miticon.geodis.geodis.Exceptions.Advisor;

import com.miticon.geodis.geodis.Exceptions.PostCodeNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class PostCodeNotFoundAdvice {

    @ResponseBody
    @ExceptionHandler(PostCodeNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String employeeNotFoundHandler(PostCodeNotFoundException ex) {
        return ex.getMessage();
    }
}
