package com.miticon.geodis.geodis.Services;

import com.miticon.geodis.geodis.Entities.UKPostCode;
import com.miticon.geodis.geodis.Exceptions.PostCodeNotFoundException;

public interface UKPostCodeService {

    UKPostCode updateCoordinatesByPostCode(String postcode, double latitude, double longitude) throws PostCodeNotFoundException;
}