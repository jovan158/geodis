package com.miticon.geodis.geodis.Services;

import com.miticon.geodis.geodis.Entities.UKPostCode;
import com.miticon.geodis.geodis.Exceptions.PostCodeNotFoundException;
import com.miticon.geodis.geodis.Repositories.UKPostCodeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UKPostCodeServiceImpl implements UKPostCodeService {

    private UKPostCodeRepository ukPostCodeRepository;

    @Autowired
    public UKPostCodeServiceImpl(UKPostCodeRepository ukPostCodeRepository) {
        this.ukPostCodeRepository = ukPostCodeRepository;
    }

    @Override
    public UKPostCode updateCoordinatesByPostCode(String postcode, double latitude, double longitude) throws PostCodeNotFoundException {
        final Optional<UKPostCode> postCode = ukPostCodeRepository.findByPostcode(postcode);
        final UKPostCode ukPostCode = postCode.orElseThrow(PostCodeNotFoundException::new);

        ukPostCode.setLatitude(latitude);
        ukPostCode.setLongitude(longitude);
        ukPostCodeRepository.save(ukPostCode);

        return ukPostCode;
    }
}
