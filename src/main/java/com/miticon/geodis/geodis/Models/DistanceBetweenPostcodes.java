package com.miticon.geodis.geodis.Models;

public class DistanceBetweenPostcodes {

    public DistanceBetweenPostcodes(double distance, String unit) {
        this.distance = distance;
        this.unit = unit;
    }

    private final double distance;
    private final String unit;

    public double getDistance() {
        return distance;
    }

    public String getUnit() {
        return unit;
    }
}
