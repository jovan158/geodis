package com.miticon.geodis.geodis.Controllers;

import com.miticon.geodis.geodis.Entities.UKPostCode;
import com.miticon.geodis.geodis.Exceptions.PostCodeNotFoundException;
import com.miticon.geodis.geodis.Services.UKPostCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UKPostCodeController {

    private final UKPostCodeService ukPostCodeService;

    @Autowired
    public UKPostCodeController(UKPostCodeService ukPostCodeService) {
        this.ukPostCodeService = ukPostCodeService;
    }

    @PutMapping("/postcodes/{postcode}")
    public UKPostCode updateCoordinatesByPostCode(@PathVariable("postcode") String postcode,
                                                  @RequestParam("latitude") double latitude,
                                                  @RequestParam("longitude") double longitude) throws PostCodeNotFoundException {
        return ukPostCodeService.updateCoordinatesByPostCode(postcode, latitude, longitude);
    }
}
