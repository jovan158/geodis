package com.miticon.geodis.geodis.Controllers;

import com.miticon.geodis.geodis.Entities.UKPostCode;
import com.miticon.geodis.geodis.Exceptions.PostCodeNotFoundException;
import com.miticon.geodis.geodis.Models.DistanceBetweenPostcodes;
import com.miticon.geodis.geodis.Repositories.UKPostCodeRepository;
import com.miticon.geodis.geodis.Utilities.DistanceUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PostcodeDistanceController {

    private final UKPostCodeRepository ukPostCodeRepository;

    @Autowired
    public PostcodeDistanceController(UKPostCodeRepository ukPostCodeRepository) {
        this.ukPostCodeRepository = ukPostCodeRepository;
    }

    @GetMapping("/distance")
    public DistanceBetweenPostcodes calculateDistanceBetweenPostcodes(@RequestParam("firstPostcode") String firstPostcode,
                                                                      @RequestParam("secondPostcode") String secondPostcode) throws PostCodeNotFoundException {
        final UKPostCode first =
                ukPostCodeRepository
                        .findByPostcode(firstPostcode)
                        .orElseThrow(PostCodeNotFoundException::new);

        final UKPostCode second =
                ukPostCodeRepository
                        .findByPostcode(secondPostcode)
                        .orElseThrow(PostCodeNotFoundException::new);

        final double distanceInKm =
                DistanceUtils
                        .calculateDistanceInKm(
                                first.getLatitude(),
                                first.getLongitude(),
                                second.getLatitude(),
                                second.getLongitude());

        return new DistanceBetweenPostcodes(distanceInKm, "km");
    }
}
