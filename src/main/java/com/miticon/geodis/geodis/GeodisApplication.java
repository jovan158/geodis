package com.miticon.geodis.geodis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GeodisApplication {

	public static void main(String[] args) {
		SpringApplication.run(GeodisApplication.class, args);
	}
}