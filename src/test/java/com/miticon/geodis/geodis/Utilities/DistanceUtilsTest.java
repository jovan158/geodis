package com.miticon.geodis.geodis.Utilities;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class DistanceUtilsTest {

    private double lat1;
    private double long1;
    private double lat2;
    private double long2;
    private double temp;

    public DistanceUtilsTest(double lat1, double long1, double lat2, double long2, double temp) {
        this.lat1 = lat1;
        this.long1 = long1;
        this.lat2 = lat2;
        this.long2 = long2;
        this.temp = temp;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {57.14870708, -2.097806027, 57.10155692, -2.268485752, 11.559144516398517},
                {57.147428, -2.1472662, 57.151797, -2.185398, 2.350757074301658},
                {57.14416516, -2.114847768, 57.13562422, -2.175239142, 3.7653435719633057},
                {57.15400596, -2.22440188, 57.17384698, -2.181539371, 3.3979838037282972},
        });
    }

    @Test
    public void calculateDistanceInKm() {
    }
}