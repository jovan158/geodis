package com.miticon.geodis.geodis.Services;


import com.miticon.geodis.geodis.Entities.UKPostCode;
import com.miticon.geodis.geodis.Exceptions.PostCodeNotFoundException;
import com.miticon.geodis.geodis.Repositories.UKPostCodeRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UKPostCodeServiceImplTest {

    @Mock
    private UKPostCodeRepository ukPostCodeRepository;

    @InjectMocks
    private UKPostCodeServiceImpl ukPostCodeService;

    @Test
    public void updateCoordinatesByPostCode() throws PostCodeNotFoundException {

        UKPostCode postCodeCoor1 = new UKPostCode("AB10 6RN", 57.13787976, -2.121486688);

        when(ukPostCodeRepository.findByPostcode("AB10 6RN")).thenReturn(Optional.of(postCodeCoor1));

        Optional<UKPostCode> temp = Optional.ofNullable(ukPostCodeService.updateCoordinatesByPostCode("AB10 6RN", 96.96, 69.69));
        Optional<UKPostCode> postCodeCoor2 =  Optional.of(temp.get());

        assertEquals(postCodeCoor2.get().getPostcode(), postCodeCoor1.getPostcode());
        assertEquals(postCodeCoor2.get().getLatitude(), postCodeCoor1.getLatitude(), 0);
        assertEquals(postCodeCoor2.get().getLongitude(), postCodeCoor1.getLongitude(), 0);

    }
}